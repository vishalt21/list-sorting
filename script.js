const courses = [
    {
        courseName: "Complete ReactJS Course",
        price: 2.8
    },
    {
        courseName: "Complete Angular Course",
        price: 2.4
    },
    {
        courseName: "Complete C++ Course",
        price: 2.1
    },
    {
        courseName: "Complete NodeJS Course",
        price: 3.2
    },
    {
        courseName: "Complete Frontend Development Course",
        price: 5.8
    },
]

function generateList() {
    const ul = document.querySelector('.list-group');
    ul.innerHTML = "";
    courses.forEach((course) => {
        //Create List Item
        const li = document.createElement('li');
        li.classList.add('list-group-item')
        const name = document.createTextNode(course.courseName)
        li.appendChild(name);

        //Create Span for price
        const span = document.createElement('span');
        span.classList.add('pull-right')
        const price = document.createTextNode('$' + ' ' + course.price)
        span.appendChild(price);

        //Inject span Into List Item
        li.appendChild(span);

        //Inject List Item in Unorder list holder
        ul.appendChild(li);
    })
}

window.addEventListener("load", generateList());

const sortLowToHigh = document.querySelector('.sortlow-to-high');
const sortHightToLow = document.querySelector('.sorthigh-to-low');

sortLowToHigh.addEventListener("click", () => {
    courses.sort((a, b) => a.price - b.price);
    generateList();
});

sortHightToLow.addEventListener("click", () => {
    courses.sort((a, b) => b.price - a.price);
    generateList();
});